#include "WiFiEsp.h"
#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial Serial1(8, 9); // RX, TX
int pinRelay = 4;
#endif

char ssid[] = "Ap Shopfloor C1";
char pass[] = "apshopfloorc1572";
int status = WL_IDLE_STATUS;
char server[] = "192.168.5.12";

unsigned long lastConnectionTime = 0;
const unsigned long postingInterval = 10000L;
WiFiEspClient client;

void setup()
{
  Serial.begin(115200);
  Serial1.begin(9600);
  WiFi.init(&Serial1);
  pinMode(4, OUTPUT);
  
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true);
  }

  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
  }
  
  Serial.println("You're connected to the network");
  printWifiStatus();
}

void loop()
{
  while (client.available()) {
    char c = client.read();
    Serial.write(c);
  }
  
  if(digitalRead(4) == 1){
    httpRequest();
  }
//  delay(1500);
}

void httpRequest()
{
  Serial.println();
  client.stop();

  if (client.connect(server, 805)) {
      Serial.println("Connecting...");
      Serial.println("----------------\n");
      String url = "/Machine/C1/insert";
      url += "?counter=1";
        client.println("GET " + url +" HTTP/1.1");
        client.println("Host: 192.168.5.12");
        client.println("Connection: close");
        client.println();
      lastConnectionTime = millis();
  }else {
      Serial.println("Connection failed");
  }
}

void printWifiStatus()
{
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
